import React from 'react';
import Modal from "../Modal";
import history from "../../history";
import {connect} from "react-redux";
import {fetchStream, deleteStream} from "../../action";
import {Link} from "react-router-dom";

class StreamDelete extends React.Component {
    componentDidMount() {
        this.props.fetchStream(this.props.match.params.id);
    }

    renderActions = () => {
        return (
            <React.Fragment>
                <button className={'ui negative button'} onClick={() => this.props.deleteStream(this.props.match.params.id)}>Delete</button>
                <Link className={'ui button'} to='/'>Cancel</Link>
            </React.Fragment>
        );
    };

    renderContent () {
        if (!this.props.stream) {
            return 'Sure want to delete stream ?';
        }
        return 'Sure want to delete stream' + this.props.stream.title;
    }

    render() {
        return <Modal
                title='Delete stream'
                content={this.renderContent()}
                actions={this.renderActions()}
                onDismiss={() => history.push('/')}
            />
    };
};

const mapStateToProps = (state, ownProps) => {
    return {stream: state.streams[ownProps.match.params.id]}
};

export default connect(mapStateToProps, {fetchStream: fetchStream, deleteStream: deleteStream})(StreamDelete);